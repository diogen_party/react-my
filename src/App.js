import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Game from './marble/Game'
import ProductList from './ProductList'
import RightList from './RightList'
import StatePanel from './StatePanel'
import Demo1 from './Demo1'
import { render } from 'react-dom'
import Gallery from 'react-grid-gallery'


const IMAGES =
[{
        src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
        thumbnail: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 174,
        isSelected: true,
        caption: "After Rain (Jeshu John - designerspics.com)"
},
{
        src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
        thumbnail: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212,
        tags: [{value: "Ocean", title: "Ocean"}, {value: "People", title: "People"}],
        caption: "Boats (Jeshu John - designerspics.com)"
},

{
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
}]


class App extends Component {



  render() {

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>

        <StatePanel />


      <div className="container">

        <div className="row">

        <Game />
{/*}          <RightList />
          <Demo1 />
          <Demo1 images={IMAGES} />


          <ProductList />
*/}
        </div>

      </div>

    </div>



    );
  }
}

export default App;
