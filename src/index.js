import React from 'react';
import ReactDOM from 'react-dom';
// import store from 'store/index';
import { createStore } from "redux";
import { Provider } from "react-redux";

import App from './App';
import './index.css';
// import store from './TodoStore';
import reducer from "./store/reducer";

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());



// store={store}
  // 

ReactDOM.render(
	<Provider store={store}>
  	<App />
  </Provider>,
  document.getElementById('root')
);
