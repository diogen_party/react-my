import React, { Component } from 'react';
import Cell from './Cell'
import _ from 'lodash'


import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../store/actions'
// import { addArticle } from './store/actions';


class Game extends Component {

  constructor(props) {
    super(props)

    this.state = {
      point: props.point
    }
  }



  render() {

    let { items } = this.props


  console.log ('Game::render ')

    return (

  

          <div className="col-md-10">

            <div className="container-fluid">

              <div className="row justify-content-around">

                { 
                  _.map(items, (cell, id) => {

                    return (
                      <Cell 
                        key={cell.id} 
                        data={cell}
                        point={cell.value === 1}
                        actions={this.props.actions}
                      />
                    )

                  }) 
                }



              </div>
            </div>
          </div>
    )
  }



}

let mapStateToProps = (state) => {

      console.log('GAME::mapStateToProps')


  return {
    items:      state.marble.field,
    point:      state.marble.point,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Game)
