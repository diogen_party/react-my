import React, { PureComponent } from 'react'
import _ from 'lodash'
// import { connect } from 'react-redux'
// import { bindActionCreators } from 'redux'
// import * as actions from './store/actions'

class Cell extends PureComponent {

  constructor(props) {
    super(props)

    this.state = {

    }
  }

  tiltDown() {

    this.props.actions.tiltDown()
  }

  render() {
  // console.log ('Cell::render ')

  	let { data } = this.props

    let point_color = data.value === 1 ? 'green' : '#edf3f8'

  	return (
        <div className="col-md-3" onClick={this.tiltDown.bind(this)}>
          <div className="card mb-3 box-shadow" style={{ backgroundColor: '#edf3f8' }}>
            <img className="card-img-top " alt='title' src="" style={{"height": "100px", 'width': '100px', 'display': 'block', 'backgroundColor': point_color, 'marginTop': '4px', 'align': 'center'}} />
            <div className="card-body">
              <p className="card-text"><b>{data.value}</b></p>
                <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  
                </div>
              </div>
            </div>
          </div>
        </div>

		)

  }

}


export default Cell
