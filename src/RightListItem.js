import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './store/actions'
import _ from 'lodash'

class RightListItem extends Component {


  constructor(props) {
    super(props)

    this.state = {
      bold: false
    }
  }

  onClick() {


  }

  filterProducts(list_id) {

    this.props.actions.filterProductsBy(list_id)

  }

  onMouseEnter(list_id) {
    
    this.setState({
      bold: true
    })

    this.props.actions.illuminateProductsBy(list_id) // для кого? для лист_айтем или для листа?

  }

  onMouseLeave() {

    this.setState({
      bold: false
    })
  }


            // onMouseEnter={ this.illuminateLists.bind(this, item.id) }
            // onMouseLeave={ this.obscureLists.bind(this, item.id) }
  render() {

    let { item } = this.props

      let style_bold = this.state.bold ? { fontWeight: 'bold' } : {};
      // console.log( style_bold )

      return (
          <li className={ item.li_classes } 
            onClick={ this.filterProducts.bind(this, item.id) }
            onMouseEnter={ this.onMouseEnter.bind(this, item.id) }
            onMouseLeave={ this.onMouseLeave.bind(this) }
          >
            <div onClick={ this.onClick.bind(this) }>
              <h6 className="my-0" style={ style_bold }>{ item.title }</h6>
              <small className="text-muted">{ item.desc }</small>
            </div>
            <span className="text-muted">{ item.cost }</span>
          </li>
      )

  }

}


let mapStateToProps = (state) => {


  return {
  }
}

let mapDispatchToProps = (dispatch) => {

  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightListItem)
