


export const changeList = (data) => {
	return {
		type: 'CHANGE_LIST',
		payload: data
	}
}

export const changeList2 = (data) => {
	return {
		type: 'CHANGE_LIST2',
		payload: 'hello'
	}
}

export const addArticle = (data) => {

	return { 
		type: 'ADD_ARTICLE', 
		payload:  data
	}
}

export const addList = (title) => {

	return { 
		type: 'ADD_LIST', 
		payload:  {
			title: title
		}
	}
}

export const illuminateProductsBy = (list_id) => {

	return {
		type: 'ILLUMINATE_PRODUCTS', 
		payload:  {
			list_id: list_id
		}
	}
}

export const obscureProductsBy = (list_id) => {

	return {
		type: 'OBSCURE_PRODUCTS', 
		payload:  {
			list_id: list_id
		}
	}
}

export const filterProductsBy = (list_id) => {

	return {
		type: 'FILTER_PRODUCTS', 
		payload:  {
			list_id: list_id
		}
	}
}




export const excludeProductFromList = (product_id) => {

	return {
		type: 'EXCLUDE_PRODUCT_FROM_LIST', 
		payload:  {
			product_id: product_id
		}
	}
}


export const includeProductToList = (product_id) => {

	return {
		type: 'INCLUDE_PRODUCT_TO_LIST', 
		payload:  {
			product_id: product_id
		}
	}
}

export const archiveProduct = (product_id) => {

	return {
		type: 'ARCHIVE_PRODUCT', 
		payload:  {
			product_id: product_id
		}
	}
}





export const tiltDown = (product_id) => {

	return {
		type: 'MARBLE_TILT_DOWN', 
		payload:  {
		}
	}
}





// export const addArticle = article => ({ type: ADD_ARTICLE, payload: article });

