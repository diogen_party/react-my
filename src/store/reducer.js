import _ from 'lodash'

let all_items = _.map(_.rangeRight(1, 12), number => {
  return {
    id: number,
    title: 'Title ' + number
  }
})
console.log(all_items)

// wrap items with visual-data

all_items = _.reduce(all_items, (result, item, key) => {

    result[item.id] = Object.assign(item, {
      is_archive: false // вариант с Visual-data перешел в список архивных записей
    })

    return result
  }, {})

// console.log(all_items[8])

// -----          MARBLE  

let cells = _.map(_.rangeRight(0, 9), number => {
  return {
    id: number,
    value: 0
  }
})

cells[2].value = 1
/*cells = _.reduce(cells, (result, item, key) => {

    result[item.id] = Object.assign(item, {
      is_archive: false // вариант с Visual-data перешел в список архивных записей
    })

    return result
  }, {})
*/

const initialState = {

  items: all_items,
  lists: {
	  // current: _.map(_.keys(all_items), (item) => parseInt(item)), //[1,2,3],
  	// all:    _.keys(all_items),
    lists_products_rels: {
      1: [1,2,8],
      2: [3,4,5,9],
      3: [3,5,7,11],
      4: [1,3,4,10,7,6],
    },


    archived: [],
  },

  ui: {
    filter_list_id: 0,
    illuminate_list_id: 0,
  },


  marble: {
    field: cells,
    point: 2
  }

  // ui_list_id: 0
}
console.log (cells)



const reducer = (state = initialState, action) => {

  switch (action.type) {

    case 'CHANGE_LIST':

      return {
        ...state,
        lists: {
          ...state.lists,
          current: [2,3]
        },
      }

    case 'CHANGE_LIST2':

      return {
      	...state,
      	lists: {
      		...state.lists,
      		current: [4,5,3]
      	},
      }



    case 'ADD_ARTICLE':

      // state.items['ff'] = action.payload

        let items = state.items
        // console.log( items )
        items['ddd'] = action.payload

      return {
        ...state,
        items 
      }

      // return { ...state, items: ...state.items.assign({ action.payload.key} };

    case 'ADD_LIST':


    	let list = {
        id: 22,
        title: action.payload.title,
      }

      // let all = state.lists.all

      // all['fd'] = list

      return {
      	...state,
      	lists: {
          ...state.lists,
          all: {
            ...state.lists.all,
            fd: list
          }
        } 
      }


    case 'ILLUMINATE_PRODUCTS':

      // console.log('illuminate ' + action.payload.list_id)

      return {
        ...state,
        ui: {
          ...state.ui,
          illuminate_list_id: action.payload.list_id
        }
      }


    case 'OBSCURE_PRODUCTS':

    // console.log('obscure ' + action.payload.list_id)

      // еcли выбран один из списков
      if (state.ui.filter_list_id > 0) {

        return {
          ...state,
          ui: {
            ...state.ui,
            illuminate_list_id: state.ui.filter_list_id // same as filter_id
          }
        }

      }

      // не выделять элементы

      return {
        ...state,
        ui: {
          ...state.ui,
          illuminate_list_id: 0
        }
      }

    case 'FILTER_PRODUCTS':

      console.log('FILTER_PRODUCTS, list_id ' + action.payload.list_id)

      if (state.lists.lists_products_rels[action.payload.list_id] === undefined) {

        return {
          ...state,
          ui: {
            ...state.ui,
            filter_list_id: 0
          }
        }
      }


      return {
        ...state,
        ui: {
          ...state.ui,
          filter_list_id: action.payload.list_id
        }
      }







    case 'EXCLUDE_PRODUCT_FROM_LIST':

      // console.log('EXCLUDE_PRODUCT_FROM_LIST, product_id: ' + action.payload.product_id)

      if (state.ui.illuminate_list_id === 0) {

        console.log('ERROR')
        return state
      }
      console.log('exclude ' + action.payload.product_id)


// убираем из связей список-товары идешник_товара
      let new_list = _.without( state.lists.lists_products_rels[state.ui.illuminate_list_id], action.payload.product_id)
// console.log(new_list)
// вместо копирования и подмены как в императиве-переменной

      return {
        ...state,
        lists: {
          ...state.lists,

          lists_products_rels: {
            ...state.lists.lists_products_rels,
            [state.ui.illuminate_list_id]: new_list
          }
        }
      }



    case 'INCLUDE_PRODUCT_TO_LIST':

      // console.log('INCLUDE_PRODUCT_TO_LIST, product_id: ' + action.payload.product_id)

      if (state.ui.illuminate_list_id === 0) {

        console.log('ERROR')
        return state
      }


      let new_list2 = state.lists.lists_products_rels [state.ui.illuminate_list_id].concat( action.payload.product_id )
      console.log(new_list2)
      console.log('include ' + action.payload.product_id)


      return {
        ...state,
        lists: {
          ...state.lists,

          lists_products_rels: {
            ...state.lists.lists_products_rels,
            [state.ui.illuminate_list_id]: new_list2
          }
        }
      }



    case 'ARCHIVE_PRODUCT':

// chge prop by item
/*      let archived = state.items[action.payload.product_id]
      archived.is_archive = true
      console.log('archive ' + action.payload.product_id)
      // delete state.items[action.payload.product_id]
*/

      let archived_list = state.lists.archived.concat( action.payload.product_id )
      console.log(archived_list)
      console.log('archive ' + action.payload.product_id)

      return {
        ...state,
        lists: {
          ...state.lists,
          archived: archived_list
        }
      }



    case 'MARBLE_TILT_DOWN':

      console.log('MARBLE_TILT_DOWN ' )

      let cells = state.marble.field

      let old_point = state.marble.point
      let new_point = old_point + 1

      cells[old_point].value = 0
      cells[new_point].value = 1

      return {
        ...state,
        marble: {
          // ...state.marble,
          field: cells,
          point: new_point
        }
      }





    default:
      return state;
  }
};





// const rootReducer = (state = initialState, action) => state

export default reducer