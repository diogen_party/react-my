import { autorun, observable, action, decorate } from "mobx"

class TodoStore {

	a = "aaa"
	b = "bbb"

	tickA() {

		this.a = this.a + 'A'
	}

	tickB() {
		this.b  = this.b + 'B'
	}

}

var store = window.store = new TodoStore()

decorate(TodoStore, {
	a: observable,
	b: observable,
	tickA: action,
	tickB: action,

})
export default TodoStore
/*let store = {
}*/
autorun( () => {
	console.log(store.a)
	console.log(store.b)
})