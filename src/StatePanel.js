import React, { Component } from 'react';
import _ from 'lodash'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './store/actions'


class StatePanel extends Component {


  render() {


    return (



      <div>

        <span>FILTER_LIST_ID: {this.props.filter_list_id}</span>
        <br/>
        <span>ILLUMINATE_LIST_ID: {this.props.illuminate_list_id}</span>
        <br/>
        <span>MARBLE_POINT: {this.props.marble_point}</span>

      </div>
    )
  }



}

let mapStateToProps = (state) => {


  return {
    filter_list_id:      state.ui.filter_list_id,
    illuminate_list_id:  state.ui.illuminate_list_id,
    marble_point:        state.marble.point
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatePanel)
