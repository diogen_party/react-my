import React, { Component } from 'react';
import RightListItem from './RightListItem';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './store/actions'
import _ from 'lodash'

class RightList extends Component {


  changeList(list_id) {

    console.log ('list_id ', list_id)
    this.props.actions.changeList(list_id)
  }

  addList(event) {

    event.preventDefault()

    this.props.actions.addList( this.refs.list_title.value )

  }

  illuminateLists(list_id) {

    // console.log('illuminate ' + product_id)
    this.props.actions.illuminateProductsBy(list_id)
  }

  obscureLists(list_id) {

    this.props.actions.obscureProductsBy(list_id)
  }



  render() {
    
    let lists_data = [

      {
        id: 1,
        li_classes: 'list-group-item d-flex justify-content-between lh-condensed',
        title: 'Product name',
        desc: 'Brief description',
        cost: '$12'
      },
      {
        id: 2,
        li_classes: 'list-group-item d-flex justify-content-between lh-condensed',
        title: 'Second product',
        desc: 'Brief description',
        cost: '$8'
      },
      {
        id: 3,
        li_classes: 'list-group-item d-flex justify-content-between lh-condensed',
        title: 'Third product',
        desc: 'Brief description',
        cost: '$5'
      },
      {
        id: 4,
        li_classes: "list-group-item d-flex justify-content-between bg-light",
        title: 'Promo code',
        desc: 'EXAMPLECODE',
        cost: '$5'
      },
      {
        id: 5,
        li_classes: "list-group-item d-flex justify-content-between",
        title: 'Promo code',
        desc: 'Total (USD)',
        cost: '$20'
      },
    ]





/*    let list_items_apply = _.map(lists_data, (item) => {
  
      return (
          <li key={ item.id } className={ item.li_classes } 
            onMouseEnter={ this.illuminateLists.bind(this, item.id) }
            onMouseLeave={ this.obscureLists.bind(this, item.id) }
            onClick={ this.filterProducts.bind(this, item.id) }
          >
            <div onClick={ () => { this.state = { bold: true} }  }>
              <h6 className="my-0" style={this.state.bold?{ fontWeight: 'bold' }:{}}>{ item.title }</h6>
              <small className="text-muted">{ item.desc }</small>
            </div>
            <span className="text-muted">{ item.cost }</span>
          </li>
      )

    })
*/
/*    let list_items = (

        <ul className="list-group mb-3">

          <li className="list-group-item d-flex justify-content-between lh-condensed">
            <div onClick={ this.changeList.bind(this) }>
              <h6 className="my-0">Product name</h6>
              <small className="text-muted">Brief description</small>
            </div>
            <span className="text-muted">$12</span>
          </li>
          <li className="list-group-item d-flex justify-content-between lh-condensed">
            <div onClick={ this.changeList2.bind(this) }>
              <h6 className="my-0">Second product</h6>
              <small className="text-muted">Brief description</small>
            </div>
            <span className="text-muted">$8</span>
          </li>
          <li className="list-group-item d-flex justify-content-between lh-condensed">
            <div>
              <h6 className="my-0">Third item</h6>
              <small className="text-muted">Brief description</small>
            </div>
            <span className="text-muted">$5</span>
          </li>
          <li className="list-group-item d-flex justify-content-between bg-light">
            <div className="text-success">
              <h6 className="my-0">Promo code</h6>
              <small>EXAMPLECODE</small>
            </div>
            <span className="text-success">-$5</span>
          </li>
          <li className="list-group-item d-flex justify-content-between">
            <span>Total (USD)</span>
            <strong>$20</strong>
          </li>

        </ul>
    )
*/

    return (

      <div className="col-md-2">
        <h4 className="d-flex justify-content-between align-items-center mb-3">
          <span className="text-muted">Your cart2</span>
          <span className="badge badge-secondary badge-pill">3</span>
        </h4>

        <ul>
        {
          _.map(lists_data, (item) => {
  
            return (<RightListItem key={ item.id } item={item} />)
          })
        }
        </ul>

        <form className="card p-2">
          <div className="input-group">
            <input ref="list_title" type="text" className="form-control" placeholder="Promo code" />
            <div className="input-group-append">
              <button type="submit" className="btn btn-secondary" onClick={ this.addList.bind(this) }>Redeem</button>
            </div>
          </div>
        </form>
      </div>



    );
  }
}


let mapStateToProps = (state) => {



  return {
    current:   state.lists.current,
  }
}

let mapDispatchToProps = (dispatch) => {

  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightList)
