import React, { PureComponent } from 'react'
import _ from 'lodash'
// import { connect } from 'react-redux'
// import { bindActionCreators } from 'redux'
// import * as actions from './store/actions'

class ProductListItem extends PureComponent {

  constructor(props) {
    super(props)

    this.state = {

      is_obscure: this.props.is_obscure,
    }
  }

  excludeItemFromList() {

    let { data } = this.props

    this.props.actions.excludeProductFromList(data.id)

    this.setState({ is_obscure: true })
  }

  includeItemToList() {

    let { data } = this.props

    this.props.actions.includeProductToList(data.id)

    this.setState({ is_obscure: false })
  }

  archiveProduct() {

    let { data } = this.props

    this.props.actions.archiveProduct(data.id)
  }


  render() {

  	let { data, is_obscure } = this.props

/*    if (data.id === 1) {
      console.log ('render 1')
    }
    if (data.id === 2) {
      console.log ('render 2')
    }
*/    let bg_color = is_obscure ? '#c6ffb3' : '#edf3f8'
    // console.log('bg_color ' + bg_color)

  	return (
        <div className="col-md-3">
          <div className="card mb-3 box-shadow" style={{ backgroundColor: bg_color}}>
            <img className="card-img-top " alt='title' src="" style={{"height": "150px", 'width': '150px', 'display': 'block', 'backgroundColor': 'blue', 'marginTop': '4px', 'align': 'center'}} />
            <div className="card-body">
              <p className="card-text">{data.title}</p>
                <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">

                {
                  is_obscure ? 

                  ( <button type="button" style={{backgroundColor: 'SandyBrown'}} className="btn btn-sm btn-outline-secondary" onClick={this.excludeItemFromList.bind(this)}>Exclude</button> )
                  :
                  ( <button type="button" style={{backgroundColor: 'SpringGreen'}} className="btn btn-sm btn-outline-secondary" onClick={this.includeItemToList.bind(this)}>Include</button> )
                }
                  
                  <button type="button" className="btn btn-sm btn-outline-secondary" onClick={this.archiveProduct.bind(this)}>To Archive</button>
                </div>
                <small className="text-muted">m</small>
              </div>
            </div>
          </div>
        </div>

		)

  }

}


export default ProductListItem
