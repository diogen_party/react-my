import React, { Component } from 'react';
import ProductListItem from './ProductListItem'
import _ from 'lodash'


import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './store/actions'
// import { addArticle } from './store/actions';


class ProductList extends Component {



  render() {

    let { items/*, actions, ui_list_id*/ } = this.props

    var { products_illuminate_ids, archived_ids } = this.props

    let current = _.map(_.keys(items), (item) => parseInt(item, 10)) //[1,2,3],
    // console.log ('products_ids: ' + current)
    // console.log ('illuminate_ids: ' + products_illuminate_ids)

    let showing_items =  _.reduce(items, (result, item, key) => {

      if (! _.includes(archived_ids, parseInt(item.id, 10))) {

        result[item.id] = item
      }

      return result
    }, {})


    return (



          <div className="col-md-10">

            <div className="container-fluid">

              <div className="row">

                { 
                  _.map(showing_items/*_.filter(items, item => {return !item.is_archive})*/, (product, id) => {

                    return (
                      <ProductListItem 
                        key={product.id} 
                        data={product} 
                        is_obscure={_.includes(products_illuminate_ids, parseInt(id, 10))}
                        actions={this.props.actions}
                      />
                    )

                  }) 
                }



              </div>
            </div>
          </div>
    )
  }



}

let mapStateToProps = (state) => {


  let items = state.items

  if (state.ui.filter_list_id > 0 ) {

    let products_ids = state.lists.lists_products_rels [state.ui.filter_list_id]

/*    items = _.filter(state.items, (item, key) => {

      return _.includes(products_ids, parseInt(key, 10))
    })
*/
    items = _.reduce(state.items, (result, item, key) => {

      if (_.includes(products_ids, parseInt(key, 10))) {

        result[key] = item
      }

      return result
    }, {})

  }

  let products_illuminate_ids = []

  if (state.ui.illuminate_list_id > 0) {

    products_illuminate_ids = state.lists.lists_products_rels [state.ui.illuminate_list_id]
  }

  return {
    items:      items,
    // current:    state.lists.current,
    // ui_list_id: state.ui.illuminate_list_id,
    products_illuminate_ids: products_illuminate_ids,
    archived_ids: state.lists.archived,
    // lists_products_rels: prods_ids
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)

/*  let items = _.reduce(state.items, (result, item, key) => {

    if (_.includes(state.lists.current, parseInt(key))) {

      result[key] = item
    }

    return result
  }, {})
*/


